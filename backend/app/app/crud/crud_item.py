from typing import List

from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session
from sqlalchemy import func

from app.crud.base import CRUDBase
from app.models.item import Item
from app.schemas.item import ItemCreate, ItemUpdate


class CRUDItem(CRUDBase[Item, ItemCreate, ItemUpdate]):
    def create_with_owner(
            self, db: Session, *, obj_in: ItemCreate, owner_id: int
    ) -> Item:
        obj_in_data = jsonable_encoder(obj_in)
        db_obj = self.model(**obj_in_data, owner_id=owner_id)
        db.add(db_obj)
        db.commit()
        db.refresh(db_obj)
        return db_obj

    def get_multi_filtered(
            self, db: Session, *,
            substr: str = None, category_id: int = None,
            approved_only: bool = True, certain_name: str = None,
            skip: int = 0, limit: int = 100
    ) -> List[Item]:
        query = db.query(self.model)
        if substr:
            query = query.filter(Item.title.ilike(f'%{substr}%'))
        if certain_name:
            query = query.filter(
                func.lower(Item.title) == (func.lower(certain_name))
            )
        if approved_only:
            query = query.filter(Item.approved.is_(approved_only))
        if category_id:
            query = query.filter(Item.category_id == category_id)
        return (
            query
                .offset(skip)
                .limit(limit)
        )

    def get_multi_childs(
            self, db: Session, *,
            item_id: int, down_tree: bool = True,
            skip: int = 0, limit: int = 100
    ) -> List[Item]:
        query = db.query(self.model)
        if down_tree:
            query = query.filter(Item.parent_id == item_id)
        else:
            query = query.filter(Item.id == item_id)
        return (
            query
                .offset(skip)
                .limit(limit)
        )


item = CRUDItem(Item)
