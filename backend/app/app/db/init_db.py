from app import crud, schemas
from app.core.config import settings
from app.db import base  # noqa: F401
from sqlalchemy.orm import Session


# make sure all SQL Alchemy models are imported (app.db.base) before initializing DB
# otherwise, SQL Alchemy might fail to initialize relationships properly
# for more details: https://github.com/tiangolo/full-stack-fastapi-postgresql/issues/28


def init_db(db: Session) -> None:
    # Tables should be created with Alembic migrations
    # But if you don't want to use migrations, create
    # the tables un-commenting the next line
    # Base.metadata.create_all(bind=engine)

    user = crud.user.get_by_email(db, email=settings.FIRST_SUPERUSER)
    if not user:
        user_in = schemas.UserCreate(
            email=settings.FIRST_SUPERUSER,
            password=settings.FIRST_SUPERUSER_PASSWORD,
            is_superuser=True,
        )
        user = crud.user.create(db, obj_in=user_in)  # noqa: F841

    if crud.category.count(db) == 0:
        with open('app/db/fixtures/categories.csv',
                  mode='r', encoding='utf-8') as f:
            for item in f:
                print(item.rstrip('\n'))
                _, cat_title, cat_desc = item.rstrip('\n').split(';')
                cat_in = schemas.CategoryCreate(
                    title=cat_title.strip("'"),
                    description=cat_desc.strip("'")
                )
                crud.category.create(db, obj_in=cat_in)

    if crud.item.count(db) == 0:
        with open('app/db/fixtures/items.csv',
                  mode='r', encoding='utf-8') as f:
            for item in f:
                _, title, cat_id, parent_id = item.rstrip('\n').split(';')
                item_in = schemas.ItemCreate(
                    title=title.strip("'"),
                    parent_id=int(parent_id) if parent_id != 'NULL' else None,
                    category_id=int(cat_id) if cat_id != 'NULL' else None,
                    approved=True
                )
                crud.item.create(db, obj_in=item_in)
