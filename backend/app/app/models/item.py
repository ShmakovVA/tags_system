from typing import TYPE_CHECKING

from app.db.base_class import Base
from sqlalchemy import Column, ForeignKey, Integer, String, Boolean, inspect
from sqlalchemy.orm import relationship, backref, collections

if TYPE_CHECKING:
    from .user import User
    from .category import Category  # noqa: F401


class Item(Base):
    id = Column(Integer, primary_key=True, index=True)
    title = Column(String, index=True, unique=True)
    description = Column(String, index=True)
    owner_id = Column(Integer, ForeignKey("user.id"))
    owner = relationship("User", back_populates="items")
    parent_id = Column(Integer, ForeignKey("item.id"))
    children = relationship(
        "Item",
        cascade="all, delete-orphan",
        backref=backref('parent', remote_side=[id]),
        collection_class=collections.attribute_mapped_collection('id')
    )
    category_id = Column(Integer, ForeignKey("category.id"))
    category = relationship("Category")
    approved = Column(Boolean, default=False, index=True)

    @property
    def weight(self):
        if self.category:
            return self.category.weight

    def as_dict(self, exclude_fields=None):
        exclude_fields = exclude_fields or []
        res = {c.key: getattr(self, c.key)
               for c in inspect(self).mapper.column_attrs
               if not c.key in exclude_fields}
        cat = res.pop('category_id', None)
        if cat and self.category:
            res['category'] = self.category.title
        return res

    def get_children(self, approved_only=True):
        children = self.children.values()
        return [child_ for child_ in children if child_.approved] \
            if approved_only else children

    def get_tree_as_dict(self, out_dict, depth, exclude_fields=None,
                         max_depth=None, parents_chain=None,
                         approved_only=True):
        """
            Populating out_dict with serialized model data recursively
            (until max_depth will be reached)
        """

        exclude_fields = [] if exclude_fields is None else exclude_fields
        parents_chain = parents_chain or ''
        if max_depth is not None and depth == max_depth:
            return
        itm = self.as_dict(exclude_fields=exclude_fields)
        current_itm_title = itm.pop('title')
        itm['items'] = {}
        parents_chain += f'{current_itm_title}>'
        out_dict.update({current_itm_title: itm, })
        items = out_dict[current_itm_title]['items']
        for child in self.get_children(approved_only=approved_only):
            itm = child.as_dict(exclude_fields=exclude_fields)
            itm_title = itm.pop('title')
            itm['parents_chain'] = f'{parents_chain}{itm_title}>'
            items.update({itm_title: itm, })
            if child.children:
                depth += 1
                child.get_tree_as_dict(out_dict=items, depth=depth,
                                       exclude_fields=exclude_fields,
                                       max_depth=max_depth,
                                       parents_chain=parents_chain,
                                       approved_only=approved_only)
