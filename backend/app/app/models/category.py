from sqlalchemy import Column, Integer, String, Float

from app.db.base_class import Base


class Category(Base):
    id = Column(Integer, primary_key=True, index=True)
    title = Column(String, index=True, unique=True)
    description = Column(String, index=True)
    weight = Column(Float)
