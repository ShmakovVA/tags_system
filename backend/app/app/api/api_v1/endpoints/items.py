from typing import Any, List

from app import crud, models, schemas
from app.api import deps
from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session
from sqlalchemy.exc import IntegrityError

router = APIRouter()


@router.get("/", response_model=List[schemas.Item])
def read_items(
        db: Session = Depends(deps.get_db),
        skip: int = 0,
        limit: int = 10000,
        substr: str = None,
        category_id: int = None,
        current_user: models.User = Depends(deps.get_current_active_user),
) -> Any:
    """
    Retrieve items.
    """
    approved_only = not crud.user.is_superuser(current_user)
    items = crud.item.get_multi_filtered(db, skip=skip, limit=limit,
                                         substr=substr,
                                         category_id=category_id,
                                         approved_only=approved_only)
    return items.all()


@router.post("/", response_model=schemas.Item)
def create_item(
        *,
        db: Session = Depends(deps.get_db),
        item_in: schemas.ItemCreate,
        current_user: models.User = Depends(deps.get_current_active_user),
) -> Any:
    """
    Create new item.
    """
    item_in.approved = crud.user.is_superuser(current_user)
    try:
        if item_in.parent_id:
            parent = crud.item.get(db=db, id=item_in.parent_id)
            if not item:
                raise HTTPException(status_code=404,
                                    detail="Parent Item not found")
            item_in.category_id = parent.category_id
        item = crud.item.create_with_owner(db=db, obj_in=item_in,
                                           owner_id=current_user.id)
    except IntegrityError as e:
        raise HTTPException(status_code=404, detail=f"{e.orig}")
    return item


@router.put("/{id}", response_model=schemas.Item)
def update_item(
        *,
        db: Session = Depends(deps.get_db),
        id: int,
        item_in: schemas.ItemUpdate,
        current_user: models.User = Depends(deps.get_current_active_user),
) -> Any:
    """
    Update an item.
    """
    item = crud.item.get(db=db, id=id)
    if not item:
        raise HTTPException(status_code=404, detail="Item not found")
    if not crud.user.is_superuser(current_user):
        raise HTTPException(status_code=400, detail="Not enough permissions")
    item = crud.item.update(db=db, db_obj=item, obj_in=item_in)
    return item


@router.get("/{id}", response_model=List[schemas.Item])
def read_item(
        *,
        db: Session = Depends(deps.get_db),
        id: int,
        with_parents: bool = False,
        with_childs: bool = False,
        current_user: models.User = Depends(deps.get_current_active_user),
) -> Any:
    """
    Get item by ID.
    """
    item = crud.item.get(db=db, id=id)
    if not item:
        raise HTTPException(status_code=404, detail="Item not found")
    else:
        if not item.approved and not crud.user.is_superuser(current_user):
            raise HTTPException(status_code=404,
                                detail="Not enough permissions")

    res = [item, ]
    if with_childs:
        res += crud.item.get_multi_childs(db=db, item_id=item.id,
                                          down_tree=True).all()
    parents = []
    if with_parents:
        res += crud.item.get_multi_childs(db=db, item_id=item.parent_id,
                                          down_tree=False).all()
    return res


@router.get("/{id}/tree", response_model=dict)
def read_tree_item(
        *,
        db: Session = Depends(deps.get_db),
        id: int,
        max_depth: int = 0,
        current_user: models.User = Depends(deps.get_current_active_user),
) -> Any:
    """
    Get items children tree as dict.
    """
    item = crud.item.get(db=db, id=id)
    if not item:
        raise HTTPException(status_code=404, detail="Item not found")
    else:
        if not item.approved and not crud.user.is_superuser(current_user):
            raise HTTPException(status_code=404,
                                detail="Not enough permissions")

    res = {}
    approved_only = not crud.user.is_superuser(current_user)
    item.get_tree_as_dict(
        out_dict=res,
        exclude_fields=['id', 'owner_id', 'parent_id'],
        depth=1,
        max_depth=max_depth,
        approved_only=approved_only
    )
    return res


@router.get("/{slug}/weights", response_model=dict)
def read_items_weights(
        *,
        db: Session = Depends(deps.get_db),
        slug: str,
        current_user: models.User = Depends(deps.get_current_active_user),
) -> Any:
    """
    Get items weights.
    """
    approved_only = not crud.user.is_superuser(current_user)

    tag_names = slug.split('<=>')
    tags = [crud.item.get_multi_filtered(db=db, certain_name=tag,
                                         approved_only=approved_only).first()
            for tag in tag_names]

    res = {tag.title: tag.weight for tag in tags if tag}
    return res


@router.delete("/{id}", response_model=schemas.Item)
def delete_item(
        *,
        db: Session = Depends(deps.get_db),
        id: int,
        current_user: models.User = Depends(deps.get_current_active_user),
) -> Any:
    """
    Delete an item.
    """
    item = crud.item.get(db=db, id=id)
    if not item:
        raise HTTPException(status_code=404, detail="Item not found")
    if not crud.user.is_superuser(current_user):
        raise HTTPException(status_code=400, detail="Not enough permissions")
    item = crud.item.remove(db=db, id=id)
    return item
