# Tags System

API service for providing tags and admin panel for managing. 

## Backend Requirements

* [Docker](https://www.docker.com/).
* [Docker Compose](https://docs.docker.com/compose/install/).

## Frontend Requirements

* Node.js (with `npm`).

## Starting

`docker-compose up -d`

## URLs

These are the URLs of project.

| Service | Production | Staging | Development |
| --- | --- | --- | --- |
| AdminPanel (`Frontend`) | https://tags.fastdev.pro | https://stag-tags.fastdev.pro | http://localhost |
| API (`Backend`) | https://tags.fastdev.pro/api/ | https://stag-tags.fastdev.pro/api/ | http://localhost/api/ |
|  |  |  |  |
| PGAdmin (`Database`) | https://pgadmin.tags.fastdev.pro | https://pgadmin.stag-tags.fastdev.pro | http://localhost:5050 |
|  |  |  |  |
| SwaggerUI (`Docs`) | https://tags.fastdev.pro/docs | https://stag-tags.fastdev.pro/docs | http://localhost/docs |
| ReDoc (`Docs`) | https://tags.fastdev.pro/redoc | https://stag-tags.fastdev.pro/redoc | http://localhost/redoc |
|  |  |  |  |
| TraefikUI (`Proxy`) | http://tags.fastdev.pro:8090 | http://stag-tags.fastdev.pro:8090 | http://localhost:8090 |
