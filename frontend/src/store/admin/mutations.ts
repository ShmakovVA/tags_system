import { IUserProfile, IItemProfile } from '@/interfaces';
import { AdminState } from './state';
import { getStoreAccessors } from 'typesafe-vuex';
import { State } from '../state';

export const mutations = {
    setUsers(state: AdminState, payload: IUserProfile[]) {
        state.users = payload;
    },
    setUser(state: AdminState, payload: IUserProfile) {
        const users = state.users.filter((user: IUserProfile) => user.id !== payload.id);
        users.push(payload);
        state.users = users;
    },

    setItems(state: AdminState, payload: IItemProfile[]) {
        state.items = payload;
    },
    setFilteredItems(state: AdminState, payload: IItemProfile[]) {
        state.filtered_items = payload;
    },
    setTreeItems(state: AdminState, payload: {}) {
        state.tree_items = payload;
    },
    setItem(state: AdminState, payload: IItemProfile) {
        const items = state.items.filter((item: IItemProfile) => item.id !== payload.id);
        items.push(payload);
        state.items = items;
    },
};

const { commit } = getStoreAccessors<AdminState, State>('');

export const commitSetUser = commit(mutations.setUser);
export const commitSetUsers = commit(mutations.setUsers);

export const commitSetItem = commit(mutations.setItem);
export const commitSetItems = commit(mutations.setItems);
export const commitSetFilteredItems = commit(mutations.setFilteredItems);

export const commitTreeSetItems = commit(mutations.setTreeItems);
