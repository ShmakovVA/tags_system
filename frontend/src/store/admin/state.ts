import { IUserProfile, IItemProfile } from '@/interfaces';

export interface AdminState {
    users: IUserProfile[];

    items: IItemProfile[];
    filtered_items: IItemProfile[];
    tree_items: {};
}
