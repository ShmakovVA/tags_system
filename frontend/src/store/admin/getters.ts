import { AdminState } from './state';
import { getStoreAccessors } from 'typesafe-vuex';
import { State } from '../state';

export const getters = {
    adminUsers: (state: AdminState) => state.users,
    adminOneUser: (state: AdminState) => (userId: number) => {
        const filteredUsers = state.users.filter((user) => user.id === userId);
        if (filteredUsers.length > 0) {
            return { ...filteredUsers[0] };
        }
    },

    adminItems: (state: AdminState) => state.items,
    adminFilteredItems: (state: AdminState) => state.filtered_items,
    adminTreeItems: (state: AdminState) => state.tree_items,
    adminOneItem: (state: AdminState) => (itemId: number) => {
        const filteredItems = state.items.filter((item) => item.id === itemId);
        if (filteredItems.length > 0) {
            return { ...filteredItems[0] };
        }
    },
};

const { read } = getStoreAccessors<AdminState, State>('');

export const readAdminOneUser = read(getters.adminOneUser);
export const readAdminUsers = read(getters.adminUsers);

export const readAdminOneItem = read(getters.adminOneItem);
export const readAdminItems = read(getters.adminItems);
export const readAdminFilteredItems = read(getters.adminFilteredItems);

export const readAdminTreeItems = read(getters.adminTreeItems);
