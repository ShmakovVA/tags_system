export interface IUserProfile {
    email: string;
    is_active: boolean;
    is_superuser: boolean;
    full_name: string;
    id: number;
}

export interface IUserProfileUpdate {
    email?: string;
    full_name?: string;
    password?: string;
    is_active?: boolean;
    is_superuser?: boolean;
}

export interface IUserProfileCreate {
    email: string;
    full_name?: string;
    password?: string;
    is_active?: boolean;
    is_superuser?: boolean;
}


export interface IItemProfile {
    id: number;
    title: string;
    description?: string;
    owner_id: number;
    approved: boolean;
    category_id?: number;
    parent_id?: number;
}

export interface IItemProfileUpdate {
    title?: string;
    description?: string;
    category_id?: number;
    parent_id?: number;
}

export interface IItemProfileCreate {
    title: string;
    description?: string;
    category_id?: number;
    parent_id?: number;
}
